package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.StationExpress;

public interface StationExpressMapper extends BaseMapper<StationExpress> {
}
