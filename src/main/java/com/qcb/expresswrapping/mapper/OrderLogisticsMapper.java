package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.OrderLogistics;

public interface OrderLogisticsMapper extends BaseMapper<OrderLogistics> {
}
