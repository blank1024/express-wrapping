package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.SysVo.SysRole;

public interface SysRoleMapper extends BaseMapper<SysRole> {
}
