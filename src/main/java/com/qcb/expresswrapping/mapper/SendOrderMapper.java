package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.SendOrder;

public interface SendOrderMapper extends BaseMapper<SendOrder> {
}
