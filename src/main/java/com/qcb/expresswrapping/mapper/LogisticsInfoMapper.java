package com.qcb.expresswrapping.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qcb.expresswrapping.entity.LogisticsInfo;

public interface LogisticsInfoMapper extends BaseMapper<LogisticsInfo> {
}
