package com.qcb.expresswrapping;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExpressWrappingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExpressWrappingApplication.class, args);
    }

}
