package com.qcb.expresswrapping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户地址信息
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-08-22:09
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class AddressInfo {
    @TableId(value = "id", type = IdType.AUTO)
    public Long id;
    @TableField(value = "accountId")
    public Long accountId;
    @NotBlank(message = "收件人姓名不能为空")
    public String name;
    @NotBlank(message = "联系电话不能为空")
    public String phone;
    @NotBlank(message = "地域信息不能为空")
    public String area;
    @NotBlank(message = "详细地址信息不能为空")
    public String address;
    public Boolean defaults;
}
