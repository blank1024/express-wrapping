package com.qcb.expresswrapping.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-08-22:42
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class StationExpress {
    @TableId(value = "id", type = IdType.AUTO)
    public Integer id;
    @TableField(value = "orderId")
    public String orderId;
    @TableField(value = "stationId")
    public long stationId;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "enterDate")
    public Date enterDate;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "leaveDate")
    public Date leaveDate;
    @TableField(value = "status")
    public Integer status;
    @TableField(exist = false)
    public StationInfo stationInfo;
    @TableField(exist = false)
    public SendOrder sendOrder;
}
