package com.qcb.expresswrapping.entity;

import com.alibaba.fastjson.annotation.JSONField;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * 物流信息
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-08-22:14
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class LogisticsInfo {
    @TableId(value = "logisticsId")
    public String logisticsId;
    @TableField(value = "courierId")
    public Long courierId;
    public Integer step;
    @TableField(value = "thisSite")
    public String thisSite;
    @TableField(value = "targetSite")
    public String targetSite;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "departTime")
    public Date departTime;
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "arriveTime")
    public Date arriveTime;
    @TableField(value = "status")
    public Integer status;
    public Boolean destination;
    @TableField(exist = false)
    public UserInfo courier;
}
