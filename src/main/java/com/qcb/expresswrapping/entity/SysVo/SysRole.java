package com.qcb.expresswrapping.entity.SysVo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-15-23:56
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class SysRole extends BaseEntity{
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "角色名称不能为空")
    private String name;

    @NotBlank(message = "角色编码不能为空")
    private String code;

    private String remark;

    private Integer status;

    @TableField(exist = false)
    private List<Long> menuIds = new ArrayList<>();
}
