package com.qcb.expresswrapping.entity.SysVo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-16-11:20
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class SysUserRole {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private Long userId;

    private Long roleId;
}
