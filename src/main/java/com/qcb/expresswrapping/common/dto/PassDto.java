package com.qcb.expresswrapping.common.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-03-0:10
 * To change this template use File | Settings | File and Code Templates.
 */
@Data
public class PassDto implements Serializable {

    @NotBlank(message = "新密码不能为空")
    private String password;
    @NotBlank(message = "旧密码不能为空")
    private String currentPass;
}
