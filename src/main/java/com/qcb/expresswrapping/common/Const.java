package com.qcb.expresswrapping.common;

/**
 * 项目常量类
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-16-21:06
 * To change this template use File | Settings | File and Code Templates.
 */

public class Const {

    public final static Integer STATUS_ON = 1;
    public final static Integer STATUS_OFF = 0;
    public final static String CAPTCHA_KEY = "captcha";
    public final static String GRANTED_AUTHORITY= "GrantedAuthority:";
    public final static String DEFAULT_PASSWORD = "888888";
    public final static String DEFAULT_AVATAR = "https://image-1300566513.cos.ap-guangzhou.myqcloud.com/upload/images/5a9f48118166308daba8b6da7e466aab.jpg";
}
