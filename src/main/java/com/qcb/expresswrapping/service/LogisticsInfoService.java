package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.LogisticsInfo;

import java.util.List;

public interface LogisticsInfoService extends IService<LogisticsInfo> {

    List<LogisticsInfo> listLogisiticsByOrderId(String orderId);
}
