package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.SysVo.SysUserRole;

public interface SysUserRoleService extends IService<SysUserRole> {
}
