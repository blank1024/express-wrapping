package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.SendOrder;
import com.qcb.expresswrapping.mapper.SendOrderMapper;
import com.qcb.expresswrapping.service.SendOrderService;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-11-17:40
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class SendOrderServiceImpl extends ServiceImpl<SendOrderMapper, SendOrder> implements SendOrderService {
}
