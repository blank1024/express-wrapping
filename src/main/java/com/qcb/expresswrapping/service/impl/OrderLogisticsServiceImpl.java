package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.OrderLogistics;
import com.qcb.expresswrapping.mapper.OrderLogisticsMapper;
import com.qcb.expresswrapping.service.OrderLogisticsService;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-12-23:07
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class OrderLogisticsServiceImpl extends ServiceImpl<OrderLogisticsMapper, OrderLogistics> implements OrderLogisticsService {
}
