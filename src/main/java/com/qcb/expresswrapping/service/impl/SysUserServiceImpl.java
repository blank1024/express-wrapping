package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.common.Const;
import com.qcb.expresswrapping.entity.SysVo.SysMenu;
import com.qcb.expresswrapping.entity.SysVo.SysRole;
import com.qcb.expresswrapping.entity.SysVo.SysUser;
import com.qcb.expresswrapping.mapper.SysUserMapper;
import com.qcb.expresswrapping.service.SysMenuService;
import com.qcb.expresswrapping.service.SysRoleService;
import com.qcb.expresswrapping.service.SysUserService;
import com.qcb.expresswrapping.utils.RedisUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-16-12:20
 * To change this template use File | Settings | File and Code Templates.
 */
@Slf4j
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Autowired
    SysRoleService sysRoleService;
    @Autowired
    SysMenuService sysMenuService;

    @Autowired
    SysUserMapper sysUserMapper;

    @Autowired
    RedisUtil redisUtil;

    @Override
    public SysUser getByUsername(String username) {
        return getOne(new QueryWrapper<SysUser>().eq("username", username));
    }

    @Override
    public String getUserAuthority(Long userId) {

        SysUser sysUser = sysUserMapper.selectById(userId);

        // ROLE_admin,ROLE_user,sys:user:list,...
        String authority = "";

        // 优先从redis缓存里获取权限信息
        if (redisUtil.hasKey(Const.GRANTED_AUTHORITY + sysUser.getUsername())){
            authority = (String)redisUtil.get(Const.GRANTED_AUTHORITY + sysUser.getUsername());
        }else {
            // 获取角色编码
            List<SysRole> roles = sysRoleService.list(new QueryWrapper<SysRole>()
                    .inSql("id", "SELECT role_id FROM sys_user_role WHERE user_id = " + userId));
            if (roles.size() > 0){
                String roleCodes = roles.stream().map(r -> "ROLE_" + r.getCode()).collect(Collectors.joining(","));
                authority = roleCodes.concat(",");
            }
            // 获取菜单操作编码
            List<Long> menuIds = sysUserMapper.getNavMenuIds(userId);
            if (menuIds.size() > 0){
                List<SysMenu> menus = sysMenuService.listByIds(menuIds);
                String menuPerms = menus.stream().map(q -> q.getPerms()).collect(Collectors.joining(","));
                authority = authority.concat(menuPerms);
            }

            log.info("用户ID - {} ---拥有的权限：{}", userId, authority);
            redisUtil.set(Const.GRANTED_AUTHORITY + sysUser.getUsername(), authority, 60 * 60);
        }

        return authority;
    }

    // 删除改用户的所有权限信息
    @Override
    public void clearUserAuthorityInfo(String username) {
        redisUtil.del(Const.GRANTED_AUTHORITY + username);
    }

    // 删除所有与该角色关联的用户的权限信息
    @Override
    public void clearUserAuthorityInfoByRoleId(Long roleId) {
        List<SysUser> sysUsers = this.list(new QueryWrapper<SysUser>()
                .inSql("id", "select user_id from sys_user_role where role_id = " + roleId));

        sysUsers.forEach(u -> {
            this.clearUserAuthorityInfo(u.getUsername());
        });
    }

    // 删除所有与该菜单关联的所有用户的权限信息
    @Override
    public void clearUserAuthorityInfoByMenuId(Long menuId) {
        List<SysUser> sysUsers = sysUserMapper.listByMenuId(menuId);

        sysUsers.forEach(u -> {
            this.clearUserAuthorityInfo(u.getUsername());
        });
    }
}
