package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.SysVo.SysRoleMenu;
import com.qcb.expresswrapping.mapper.SysRoleMenuMapper;
import com.qcb.expresswrapping.service.SysRoleMenuService;
import org.springframework.stereotype.Service;

@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements IService<SysRoleMenu>, SysRoleMenuService {

}
