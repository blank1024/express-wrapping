package com.qcb.expresswrapping.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcb.expresswrapping.entity.SysVo.SysRole;
import com.qcb.expresswrapping.mapper.SysRoleMapper;
import com.qcb.expresswrapping.service.SysRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-16-12:23
 * To change this template use File | Settings | File and Code Templates.
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

    @Override
    public List<SysRole> listRolesByUserId(Long userId) {

        List<SysRole> sysRoles = this.list(new QueryWrapper<SysRole>().inSql("id",
                "SELECT role_id FROM sys_user_role WHERE user_id = " + userId));
        return sysRoles;
    }
}
