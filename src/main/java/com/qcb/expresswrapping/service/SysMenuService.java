package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.common.dto.SysMenuDto;
import com.qcb.expresswrapping.entity.SysVo.SysMenu;

import java.util.List;

public interface SysMenuService extends IService<SysMenu> {

    List<SysMenuDto> getCurrentUserNav();

    List<SysMenu> tree();
}
