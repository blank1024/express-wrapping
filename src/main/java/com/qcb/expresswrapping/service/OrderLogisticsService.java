package com.qcb.expresswrapping.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.qcb.expresswrapping.entity.OrderLogistics;

public interface OrderLogisticsService extends IService<OrderLogistics> {
}
