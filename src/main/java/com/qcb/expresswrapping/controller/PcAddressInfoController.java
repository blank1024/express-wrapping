package com.qcb.expresswrapping.controller;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qcb.expresswrapping.common.Result;
import com.qcb.expresswrapping.entity.AddressInfo;
import com.qcb.expresswrapping.entity.SysVo.SysUser;
import com.qcb.expresswrapping.entity.UserInfo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-03-10-22:26
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
@RequestMapping("/pc/addressinfo")
public class PcAddressInfoController extends BaseController{

    @GetMapping("/list")
    @PreAuthorize("hasAuthority('pc:address:list')")
    public Result list(Principal principal) {

        List<AddressInfo> addressInfos = pcAddressInfoService.list(new QueryWrapper<AddressInfo>()
                .eq("accountId", sysUserService.getByUsername(principal.getName()).getId())
                .orderByDesc("defaults")
        );
        return Result.success(addressInfos);
    }

    @PostMapping("/save")
    @PreAuthorize("hasAuthority('pc:address:save')")
    public Result save(@Validated @RequestBody AddressInfo addressInfo, Principal principal) {
        ChangeDefault(addressInfo, principal);
        addressInfo.setAccountId(sysUserService.getByUsername(principal.getName()).getId());
        pcAddressInfoService.save(addressInfo);
        return Result.success(addressInfo);
    }

    @PostMapping("/update")
    @PreAuthorize("hasAuthority('pc:address:update')")
    public Result update(@Validated @RequestBody AddressInfo addressInfo, Principal principal) {
        ChangeDefault(addressInfo, principal);
        addressInfo.setAccountId(sysUserService.getByUsername(principal.getName()).getId());
        pcAddressInfoService.updateById(addressInfo);
        return Result.success(addressInfo);
    }

    private void ChangeDefault(@RequestBody @Validated AddressInfo addressInfo, Principal principal) {
        if (addressInfo.getDefaults()) {
            List<AddressInfo> addressInfos = pcAddressInfoService.list(new QueryWrapper<AddressInfo>()
                    .eq("accountId", sysUserService.getByUsername(principal.getName()).getId()));
            addressInfos.forEach(u -> {
                        u.setDefaults(false);
                        pcAddressInfoService.updateById(u);
                    });
        }
    }

    @PostMapping("/delete")
    @PreAuthorize("hasAuthority('pc:address:delete')")
    public Result delete(@RequestBody Long[] ids) {
        pcAddressInfoService.removeByIds(Arrays.asList(ids));
        return Result.success(MapUtil.builder()
                .put("msg", "地址信息删除成功")
                .build());
    }

    @GetMapping("/info/{id}")
    @PreAuthorize("hasAuthority('pc:address:list')")
    public Result info(@PathVariable("id") Long id) {
        AddressInfo addressInfo = pcAddressInfoService.getById(id);
        return Result.success(addressInfo);
    }

}
