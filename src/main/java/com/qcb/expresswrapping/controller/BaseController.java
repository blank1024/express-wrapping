package com.qcb.expresswrapping.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qcb.expresswrapping.service.*;
import com.qcb.expresswrapping.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.ServletRequestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 控制层父类
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-18-15:18
 * To change this template use File | Settings | File and Code Templates.
 */
public class BaseController {

    @Autowired
    HttpServletRequest request;

    @Autowired
    RedisUtil redisUtil;

    @Autowired
    SysUserService sysUserService;

    @Autowired
    SysRoleService sysRoleService;

    @Autowired
    SysMenuService sysMenuService;

    @Resource
    SysUserRoleService sysUserRoleService;

    @Resource
    SysRoleMenuService sysRoleMenuService;

    @Autowired
    UserInfoService pcUserInfoService;

    @Autowired
    AddressInfoService pcAddressInfoService;

    @Autowired
    StationExpressService scStationExpressService;

    @Autowired
    SendOrderService scSendOrderService;

    @Autowired
    StationInfoService scStationInfoService;

    @Autowired
    LogisticsInfoService logisticsInfoService;

    @Resource
    OrderLogisticsService orderLogisticsService;

    /*
     * @Description: 获取页码
     * @Param: []
     * @return: com.baomidou.mybatisplus.extension.plugins.pagination.Page
     */
    public Page getPage() {
        // 如果未提供当前页和查询条数，则默认当前页为1，条数为10
        int current = ServletRequestUtils.getIntParameter(request, "current", 1);
        int size = ServletRequestUtils.getIntParameter(request, "size", 10);

        return new Page(current, size);
    }
}
