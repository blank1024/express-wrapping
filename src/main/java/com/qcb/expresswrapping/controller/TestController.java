package com.qcb.expresswrapping.controller;

import cn.hutool.core.map.MapUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.qcb.expresswrapping.common.Result;
import com.qcb.expresswrapping.entity.AddressInfo;
import com.qcb.expresswrapping.entity.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * 测试使用
 * Created by IntelliJ IDEA.
 *
 * @author: QCB
 * @create: 2022-01-18-15:39
 * To change this template use File | Settings | File and Code Templates.
 */
@RestController
@RequestMapping("/test")
public class TestController extends BaseController{

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @GetMapping("/pass")
    public Result pass() {

        // 加密后密码
        String password = bCryptPasswordEncoder.encode("111111");
        boolean matches = bCryptPasswordEncoder.matches("111111", password);

        return Result.success(MapUtil.builder()
        .put("password", password)
        .put("matches", matches)
        .build());

    }

    @GetMapping("/info/{id}")
    public Result info(@PathVariable("id") Long id){
        UserInfo userInfo = pcUserInfoService.getById(id);
        Assert.notNull(userInfo, "找不到该用户信息");
        return Result.success(userInfo);
    }

    @GetMapping("/list")
    @PreAuthorize("hasAuthority('pc:address:list')")
    public Result list(Principal principal) {

        Page<AddressInfo> pageData = pcAddressInfoService.page(getPage(), new QueryWrapper<AddressInfo>()
                .eq("accountId", sysUserService.getByUsername(principal.getName()).getId()));

        return Result.success(pageData);
    }
}
